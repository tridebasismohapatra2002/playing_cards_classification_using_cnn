from django.shortcuts import render
from django.http import JsonResponse
from tensorflow import keras as kr
from PIL import Image
import numpy as np

def home(request):
    if request.method == 'POST':
        user_image = request.FILES.get('image')
        image = Image.open(user_image)
        model = kr.models.load_model("C:/Users/Tri's pc/CNN_project/mymodel")
        image = image.convert('L')
        image = image.resize((250 , 250))
        img_array = np.array(image)
        img_array = img_array/255
        img_array = np.expand_dims(img_array , axis = [0 , -1])
        pred = np.argmax(model.predict(img_array))
        classes = ['ace of clubs', 'ace of diamonds', 'ace of hearts', 'ace of spades', 'eight of clubs', 'eight of diamonds', 'eight of hearts', 'eight of spades', 'five of clubs', 'five of diamonds', 'five of hearts', 'five of spades', 'four of clubs', 'four of diamonds', 'four of hearts', 'four of spades', 'jack of clubs', 'jack of diamonds', 'jack of hearts', 'jack of spades', 'joker', 'king of clubs', 'king of diamonds', 'king of hearts', 'king of spades', 'nine of clubs', 'nine of diamonds', 'nine of hearts', 'nine of spades', 'queen of clubs', 'queen of diamonds', 'queen of hearts', 'queen of spades', 'seven of clubs', 'seven of diamonds', 'seven of hearts', 'seven of spades', 'six of clubs', 'six of diamonds', 'six of hearts', 'six of spades', 'ten of clubs', 'ten of diamonds', 'ten of hearts', 'ten of spades', 'three of clubs', 'three of diamonds', 'three of hearts', 'three of spades', 'two of clubs', 'two of diamonds', 'two of hearts', 'two of spades']
        data = {'prediction' : classes[pred].title()}
        return JsonResponse(data)
    return render(request  , 'testapp/New.html')
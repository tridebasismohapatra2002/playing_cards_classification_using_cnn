const fileInput = document.getElementById('file-input');
const uploadedImage = document.getElementById('uploaded-image');
const uploadButton = document.getElementById('upload-button');
const prediction = document.getElementById('result-div')

if(fileInput != null){
    fileInput.addEventListener('change', (e) => {
        const file = e.target.files[0];
        if (file) {
            const reader = new FileReader();

            reader.onload = (e) => {
                uploadedImage.innerHTML = `<img src="${e.target.result}" alt="Uploaded Image">`;
            };

            reader.readAsDataURL(file);
        }
    });

    uploadButton.addEventListener('click', () => {
        const file = fileInput.files[0];
        const csrfToken = document.querySelector('input[name = "csrfmiddlewaretoken"]').value;
        if (file) {
            const formData = new FormData();
            formData.append('image', file);
            formData.append('csrfmiddlewaretoken' , csrfToken)

            fetch('http://127.0.0.1:8000/', {
                method: 'POST',
                body: formData,
            })
            .then(response => response.json())
            .then(data => {
                prediction.innerHTML = data['prediction'];
            })
            .catch(error => {
                console.error(error);
            });
        }
    });
}